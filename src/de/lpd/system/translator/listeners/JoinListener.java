package de.lpd.system.translator.listeners;

import org.bukkit.Bukkit;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.player.PlayerJoinEvent;
import org.bukkit.plugin.java.JavaPlugin;
import de.lpd.system.main.utils.InventoryCreator;
import de.lpd.system.main.utils.ItemBuilder;
import de.lpd.system.translator.Launguages;

public class JoinListener implements Listener {
	
	public static String titel = "�6Launguage �7- �aOptions",
			             name_de = "&6German &7- &6Deutsch";
	
	public JoinListener(JavaPlugin plugin) {
		Bukkit.getScheduler().scheduleSyncRepeatingTask(plugin, new Runnable() {
			
			@Override
			public void run() {
				for(Player p : Bukkit.getOnlinePlayers()) {
					if(Launguages.getLaunguageFromPlayer(p.getUniqueId()) == null) {
						Launguages.setFirstTime(p.getUniqueId(), true);
					} else {
						System.out.println(Launguages.getLaunguageFromPlayer(p.getUniqueId()));
					}
					
					if(Launguages.firstTime(p.getUniqueId()) && !p.isDead() && !p.getOpenInventory().getTitle().equals(Launguages.ENGLISH.getTranslation(titel))) {
                        InventoryCreator inv = new InventoryCreator(Bukkit.createInventory(null, 9 * 6, titel));
						
						inv.setItem(19, new ItemBuilder(Material.LIME_CONCRETE).setDisplayName(Launguages.ENGLISH.getTranslation(name_de)).build());
						
						p.openInventory(inv.inv);
					}
				}
			}
			
		}, 1l, 1l);
	}
	
	@EventHandler
	public void onJoin(PlayerJoinEvent e) {
		/*
		try {
			Config cLaunge = new Config("players.yml", "laungauges");
			String root = e.getPlayer().getUniqueId().toString();
			
			if(!cLaunge.getCfg().contains(root)) {
				InventoryCreator inv = new InventoryCreator(Bukkit.createInventory(null, 9 * 6, titel));
				inv.createPlaceHolder(new ItemStack(Material.GRAY_STAINED_GLASS));
				
				ItemStack ide = new ItemStack(Material.GREEN_STAINED_GLASS);
				ide.getItemMeta().setDisplayName(name_de);
				inv.setItem(24, ide);
				
				e.getPlayer().openInventory(inv.inv);
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		} */
	}
	
	@EventHandler
	public void onClick(InventoryClickEvent e) {
		if(e.getWhoClicked() instanceof Player) {
			Player p = (Player) e.getWhoClicked();
			if(e.getView().getTitle().equalsIgnoreCase(titel)) {
				if(e.getCurrentItem() != null && e.getCurrentItem().getItemMeta().getDisplayName().equals(name_de)) {
					Launguages.setLaunguage(p.getUniqueId(), "DE");
					Launguages.setFirstTime(p.getUniqueId(), false);
					p.closeInventory();
				}
				e.setCancelled(true);
			}
		}
	}
	
}
