package de.lpd.system.translator;

import java.io.IOException;
import java.util.UUID;

import de.lpd.system.main.utils.Config;

public enum Launguages {
	
	ENGLISH("EN"), GERMAN("DE");
	
	private String kurz;
	
	private Launguages(String kurz) {
		this.kurz = kurz;
	}
	
	public static void setFirstTime(UUID player, boolean is) {
		String root = player + ".firsttime";
		Config cLaunge = null;
		try {
			cLaunge = new Config("players.yml", "laungauges");
		} catch (IOException e) {
			e.printStackTrace();
		}
		cLaunge.getCfg().set(root, is);
		try {
			cLaunge.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public static boolean firstTime(UUID player) {
		String root = player + ".firsttime";
		Config cLaunge = null;
		try {
			cLaunge = new Config("players.yml", "laungauges");
		} catch (IOException e) {
			e.printStackTrace();
		}
		if(cLaunge.getCfg().contains(root) && cLaunge.getCfg().getBoolean(root)) {
			return true;
		} else {
			return false;
		}
	}
	
	public static String getTranslationFromPlayer(UUID player, String text) {
		String lang = getLaunguageFromPlayer(player);
		return Launguages.findRight(lang).getTranslation(text);
	}
	
	public static String getLaunguageFromPlayer(UUID player) {
		try {
			Config cLaunge = new Config("players.yml", "laungauges");
			String root = player.toString() + ".launge";
			
			if(!cLaunge.getCfg().contains(root)) {
				setFirstTime(player, true);
				cLaunge.save();
			} else {
				return cLaunge.getCfg().getString(root);
			}
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
		
		return null;
	}
	
	public String getKurz() {
		return kurz;
	}
	
	public static void setLaunguage(UUID player, String lang) {
		try {
			Config cLaunge = new Config("players.yml", "laungauges");
			String root = player.toString() + ".launge";
			
			cLaunge.getCfg().set(root, lang);
			cLaunge.save();
			
		} catch (IOException e1) {
			e1.printStackTrace();
		}
	}
	
	public static Launguages findRight(String kurz) {
		for(Launguages c : Launguages.values()) {
			if(c.kurz.equalsIgnoreCase(kurz)) {
				return c;
			}
		}
		return null;
	}
	
	public String getTranslation(String text) {
		try {
			Config c = new Config(kurz + ".yml", "laungauges");
			
			int id = 0;
			
			for(String abc : text.split("")) {
				if(abc.equals("a")) {
					id += 1;
				} else if(abc.equals("b")) {
					id += 2;
				} else if(abc.equals("c")) {
					id += 3;
				} else if(abc.equals("d")) {
					id += 4;
				} else if(abc.equals("e")) {
					id += 5;
				} else if(abc.equals("f")) {
					id += 6;
				} else if(abc.equals("g")) {
					id += 7;
				} else if(abc.equals("h")) {
					id += 8;
				} else if(abc.equals("i")) {
					id += 9;
				} else if(abc.equals("j")) {
					id += 10;
				} else if(abc.equals("k")) {
					id += 11;
				} else if(abc.equals("l")) {
					id += 12;
				} else if(abc.equals("m")) {
					id += 13;
				} else if(abc.equals("n")) {
					id += 14;
				} else if(abc.equals("o")) {
					id += 15;
				} else if(abc.equals("p")) {
					id += 16;
				} else if(abc.equals("q")) {
					id += 17;
				} else if(abc.equals("r")) {
					id += 18;
				} else if(abc.equals("s")) {
					id += 19;
				} else if(abc.equals("t")) {
					id += 20;
				} else if(abc.equals("u")) {
					id += 21;
				} else if(abc.equals("v")) {
					id += 22;
				} else if(abc.equals("w")) {
					id += 23;
				} else if(abc.equals("x")) {
					id += 24;
				} else if(abc.equals("y")) {
					id += 25;
				} else if(abc.equals("z")) {
					id += 26;
				} else if(abc.equals("A")) {
					id += 27;
				} else if(abc.equals("B")) {
					id += 28;
				} else if(abc.equals("C")) {
					id += 29;
				} else if(abc.equals("D")) {
					id += 30;
				} else if(abc.equals("E")) {
					id += 31;
				} else if(abc.equals("F")) {
					id += 32;
				} else if(abc.equals("G")) {
					id += 33;
				} else if(abc.equals("H")) {
					id += 34;
				} else if(abc.equals("I")) {
					id += 35;
				} else if(abc.equals("J")) {
					id += 36;
				} else if(abc.equals("K")) {
					id += 37;
				} else if(abc.equals("L")) {
					id += 38;
				} else if(abc.equals("M")) {
					id += 39;
				} else if(abc.equals("N")) {
					id += 40;
				} else if(abc.equals("O")) {
					id += 41;
				} else if(abc.equals("P")) {
					id += 42;
				} else if(abc.equals("Q")) {
					id += 43;
				} else if(abc.equals("R")) {
					id += 44;
				} else if(abc.equals("S")) {
					id += 45;
				} else if(abc.equals("T")) {
					id += 46;
				} else if(abc.equals("U")) {
					id += 47;
				} else if(abc.equals("V")) {
					id += 48;
				} else if(abc.equals("W")) {
					id += 49;
				} else if(abc.equals("X")) {
					id += 50;
				} else if(abc.equals("Y")) {
					id += 51;
				} else if(abc.equals("Z")) {
					id += 52;
				} else if(abc.equals("0")) {
					id += 53;
				} else if(abc.equals("1")) {
					id += 54;
				} else if(abc.equals("2")) {
					id += 55;
				} else if(abc.equals("3")) {
					id += 56;
				} else if(abc.equals("4")) {
					id += 57;
				} else if(abc.equals("5")) {
					id += 58;
				} else if(abc.equals("6")) {
					id += 59;
				} else if(abc.equals("7")) {
					id += 60;
				} else if(abc.equals("8")) {
					id += 61;
				} else if(abc.equals("9")) {
					id += 62;
				} else if(abc.equals(":")) {
					id += 63;
				} else if(abc.equals(".")) {
					id += 64;
				} else if(abc.equals(",")) {
					id += 65;
				} else if(abc.equals("?")) {
					id += 66;
				} else if(abc.equals("�")) {
					id += 67;
				} else if(abc.equals(";")) {
					id += 68;
				} else if(abc.equals("/")) {
					id += 69;
				}
			}
			
			String root = "language." + kurz + "." + id; 
			if(c.getCfg().contains(root)) {
				return (String) c.getCfg().get(root);
			} else {
				c.getCfg().set(root, text);
				c.save();
				return text;
			}
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return null;
	}
	
}
