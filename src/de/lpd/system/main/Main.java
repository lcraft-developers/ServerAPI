package de.lpd.system.main;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import de.lpd.system.main.manager.AddonManager;
import de.lpd.system.translator.listeners.JoinListener;

public class Main extends JavaPlugin {
	
	private static Main plugin;
	private static AddonManager addonmanager;
	
	@Override
	public void onEnable() {
		plugin = this;
		addonmanager = new AddonManager(this);
		registerListener(new JoinListener(plugin));
		
		addonmanager.onEnable();
	}
	
	@Override
	public void onDisable() {
		addonmanager.onDisable();
	}
	
	public void registerListener(Listener l) {
		Bukkit.getPluginManager().registerEvents(l, Main.getPlugin());
	}
	
	public static Main getPlugin() {
		return plugin;
	}
	
	public static AddonManager getAddonmanager() {
		return addonmanager;
	}
	
}
