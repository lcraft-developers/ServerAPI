package de.lpd.system.main.manager;

import java.util.ArrayList;

import org.bukkit.Bukkit;

import de.lpd.system.main.Main;
import de.lpd.system.main.utils.Addon;

public class AddonManager {
	
	private ArrayList<Addon> addons;
	private Main main;
	
	public AddonManager(Main main) {
		this.main = main;
		addons = new ArrayList<>();
		register();
	}
	
	public void register() {
		addAddon(new de.lpd.system.permissions.Main(main));
	}
	
	public void onEnable() {
		onInit();
		Bukkit.getScheduler().scheduleSyncDelayedTask(main, new Runnable() {
			
			@Override
			public void run() {
				for(Addon c : addons) {
					c.onEnable();
				}
			}
			
		}, 1);
	}
	
	public void onInit() {
		for(Addon c : addons) {
			c.init();
		}
	}
	
	public void onDisable() {
		for(Addon c : addons) {
			c.onDisable();
		}
	}
	
	public void addAddon(Addon clas) {
		addons.add(clas);
	}
	
	public ArrayList<Addon> getAddons() {
		return addons;
	}
	
	public Main getMain() {
		return main;
	}
	
}
