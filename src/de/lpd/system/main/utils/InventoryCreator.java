package de.lpd.system.main.utils;

import org.bukkit.event.Listener;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import de.lpd.system.main.Main;

public class InventoryCreator implements Listener {
	
	public Inventory inv;
	
	public InventoryCreator(Inventory inv) {
		this.inv = inv;
		Main.getPlugin().registerListener(this);
	}
	
	public InventoryCreator createPlaceHolder(int size, ItemStack c) {
		
		for(int i = 0; i < size; i++) {
			if(inv.getItem(i) == null) {
				inv.setItem(i, c);
				System.out.println("a");
			}
		}
		
		return this;
	}
	
	public InventoryCreator setItem(int slot, ItemStack i) {
		inv.setItem(slot, i);
		return this;
	}
	
	public Inventory getInv() {
		return inv;
	}
	
}
