package de.lpd.system.main.utils;

import java.io.File;
import java.io.IOException;

import org.bukkit.configuration.file.FileConfiguration;

import de.lpd.system.main.Main;

public class Config {
	
	private FileConfiguration cfg;
	private File ordner;
	private File f;
	
	public Config(String filename, String path, String folder) throws IOException {
		String p = path + "//" + folder;
		if(path.endsWith("//")) {
			p = path + folder;
		}
		
		ordner = new File(p);
		ordner.mkdir();
		ordner.mkdirs();
		
		f = new File(ordner, filename);
		if(!f.exists()) {
			f.createNewFile();
		}
		
		
		
		cfg = org.bukkit.configuration.file.YamlConfiguration.loadConfiguration(f);
	}
	
	public Config(String filename, String folder) throws IOException {
		this(filename, Main.getPlugin().getDataFolder().getAbsolutePath(), folder);
	}
	
	public FileConfiguration getCfg() {
		return cfg;
	}
	
	public void save() throws IOException {
		cfg.save(f);
	}
	
}
