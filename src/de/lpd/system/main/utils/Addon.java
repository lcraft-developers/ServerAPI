package de.lpd.system.main.utils;

import org.bukkit.Bukkit;
import org.bukkit.event.Listener;

import de.lpd.system.main.Main;

public abstract class Addon {
	
	private static String id,
	                      prefix;
	private boolean enabled = true,
			        cannotdisable;
	private Main plugin;
	
	public Addon(String id, String prefix, boolean cannotdisable, Main plugin) {
		this.id = id;
		this.prefix = prefix;
		this.cannotdisable = cannotdisable;
		this.plugin = plugin;
	}
	
	public abstract void onEnable();
	public abstract void init();
	public abstract void onDisable();
	
	public boolean isCannotdisable() {
		return cannotdisable;
	}
	
	public String getId() {
		return id;
	}
	
	public void registerListener(Listener l) {
		Bukkit.getPluginManager().registerEvents(l, Main.getPlugin());
	}
	
}
