package de.lpd.system.main.utils;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class ItemBuilder {
	
	private ItemStack item;
	private ItemMeta iMeta;
	
	public ItemBuilder(Material m, int amount) {
		item = new ItemStack(m, amount);
		iMeta = item.getItemMeta();
	}
	
	public ItemBuilder(Material m) {
		this(m, 1);
	}
	
	public ItemBuilder setDisplayName(String name) {
		iMeta.setDisplayName(name);
		return this;
	}
	
	public ItemBuilder setAmount(int amount) {
		item.setAmount(amount);
		return this;
	}
	
	public ItemStack build() {
		item.setItemMeta(iMeta);
		return item;
	}
	
}
