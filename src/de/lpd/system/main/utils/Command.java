package de.lpd.system.main.utils;

import java.util.ArrayList;
import java.util.List;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.command.TabCompleter;
import org.bukkit.event.Listener;
import de.lpd.system.main.Main;
import de.lpd.system.translator.Launguages;

public abstract class Command implements CommandExecutor, Listener, TabCompleter {
	
	private ArrayList<Compeltion> completion;
	
	public Command(String cmd) {
		completion = new ArrayList<>();
		Main.getPlugin().getCommand(cmd).setExecutor(this);
		Main.getPlugin().registerListener(this);
	}
	
	@Override
	public boolean onCommand(CommandSender arg0, org.bukkit.command.Command arg1, String arg2, String[] arg3) {
		return run(arg0, arg1, arg2, arg3);
	}
	
	public abstract boolean run(CommandSender sender, org.bukkit.command.Command cmd, String label, String[] args);
	
	public String translate(String text, String lang) {
		return Launguages.findRight(lang).getTranslation(text);
	}
	
	public String CommandNotFound(String lang, String cmd, String... elseargs) {
		String msg = Vars.PREFIX + "�cDieser Command wurde nicht gefunden! Bitte versuche:�6";
		
		for(String c : elseargs) {
			msg = msg + " /" + cmd + " " + c;
		}
		
		return translate(msg, lang);
	}
	
	public void addCompletion(String[] args, ArrayList<String> next) {
		completion.add(new Compeltion(args, next));
	}
	
	public class Compeltion {
		
		private String[] findArgs;
		private ArrayList<String> next;
		
		public Compeltion(String[] findArgs, ArrayList<String> next) {
			this.findArgs = findArgs;
			this.next = next;
		}
		
		public String[] getFindArgs() {
			return findArgs;
		}
		
		public ArrayList<String> getNext() {
			return next;
		}
		
	}
	
	@Override
	public List<String> onTabComplete(CommandSender arg0, org.bukkit.command.Command arg1, String arg2, String[] arg3) {
		List<String> a = new ArrayList<>();
		
		// /setblock [Location] [Type]
		//    Cmd       arg0     arg1
		
		for(Compeltion c : completion) {
			if(c.findArgs == arg3) {
				for(String arg : c.getNext()) {
					a.add(arg);
				}
			}
		}
		
		return a;
	}
	
}
