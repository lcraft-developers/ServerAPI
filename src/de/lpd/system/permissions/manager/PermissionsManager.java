package de.lpd.system.permissions.manager;

import java.io.IOException;
import java.util.ArrayList;
import java.util.UUID;
import org.bukkit.event.Listener;
import de.lpd.system.main.utils.Config;
import de.lpd.system.permissions.Main;

public class PermissionsManager implements Listener {
	
	private Main plugin;
	private Config cfgConfig,
	               cfgPlayers,
	               cfgGroups,
	               cfgAllPermisions;
	
	public PermissionsManager(Main plugin) {
		this.plugin = plugin;
		try {
			cfgConfig = new Config("config.yml", "permissions");
			cfgPlayers = new Config("players.yml", "permissions");
			cfgAllPermisions = new Config("allGiveablePermissions.yml", "permissions");
			cfgGroups = new Config("groups.yml", "permissions");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPermissionToPlayer(UUID player, String permission) {
		addPermissionsToList(permission);
		cfgPlayers.getCfg().set("players." + player + ".permissions." + permission, true);
		try {
			cfgPlayers.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removePermissionToPlayer(UUID player, String permission) {
		addPermissionsToList(permission);
		cfgPlayers.getCfg().set("players." + player + ".permissions." + permission, false);
		try {
			cfgPlayers.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addGroup(String name, String PREFIX, String displayName) {
		cfgGroups.getCfg().set("group."+ name + ".prefix", PREFIX);
		cfgGroups.getCfg().set("group."+ name + ".display_name", displayName);
		cfgGroups.getCfg().set("group."+ name + ".enabled", true);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPlayertoGroup(UUID player, String name) {
		cfgGroups.getCfg().set("group."+ name + ".players." + player.toString(), true);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removePlayertoGroup(UUID player, String name) {
		cfgGroups.getCfg().set("group."+ name + ".players." + player.toString(), false);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removeGroup(String name) {
		cfgGroups.getCfg().set("group."+ name + ".enabled", false);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPermissionsToList(String permissions) {
		cfgAllPermisions.getCfg().set(permissions, null);
		try {
			cfgAllPermisions.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void addPermissionToGroup(String group, String permission) {
		addPermissionsToList(permission);
		cfgGroups.getCfg().set("group." + group + ".permissions." + permission, true);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void removePermissionToGroup(String group, String permission) {
		addPermissionsToList(permission);
		cfgGroups.getCfg().set("group." + group + ".permissions." + permission, false);
		try {
			cfgGroups.save();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public ArrayList<String> getGroupsFromPlayer(UUID player) {
		ArrayList<String> s = new ArrayList<>();
		for(String group : cfgGroups.getCfg().getConfigurationSection("group").getKeys(false)) {
			if(cfgGroups.getCfg().contains("group." + group + ".players")) {
				for(String players : cfgGroups.getCfg().getConfigurationSection("group." + group + ".players").getKeys(false)) {
					if(player.toString().equals(players)) {
						s.add(group);
					}
				}
			}
		}
		return s;
	}
	
	public ArrayList<String> getPermissionsFromPlayerGroups(UUID player) {
		ArrayList<String> groups = getGroupsFromPlayer(player);
		ArrayList<String> perm = new ArrayList<>();
		for(String group : groups) {
			String root = "group." + group + ".permissions.";
			for(String perms : cfgGroups.getCfg().getConfigurationSection(root).getKeys(false)) {
				boolean active = cfgGroups.getCfg().getBoolean("group." + group + ".permissions." + perms);
				if(active) {
					perm.add(perms);
				}
			}
		}
		
		return perm;
	}
	
	public boolean hasPermissions(UUID player, String permissions) {
		addPermissionsToList(permissions);
		if(getPermissionsFromPlayerGroups(player).contains(permissions)) {
			return true;
		}
		if(getPermissionsFromPlayerGroups(player).contains("*")) {
			return true;
		}
		String root = permissions.split(".")[0];
		for(int i = 1; i < permissions.split(".").length; i++) {
			 root = root + "." + permissions.split(".")[i];
			 
			 String cRoot = root + ".*";
			 if(getPermissionsFromPlayerGroups(player).contains(permissions)) {
				return true;
			}
			 if(getPermissionsFromPlayerGroups(player).contains(cRoot)) {
					return true;
				}
		}
		
		return false;
	}
	
}
