package de.lpd.system.permissions;

import de.lpd.system.main.utils.Addon;
import de.lpd.system.permissions.manager.PermissionsManager;

public class Main extends Addon {
	
	private Main plugin;
	private PermissionsManager permsManager;
	
	public Main(de.lpd.system.main.Main plugin) {
		super("permissions", "�7[�cPermissions�8-�6System�7] �r", true, plugin);
	}

	@Override
	public void onEnable() {
		plugin = this;
		permsManager = new PermissionsManager(plugin);
		registerListener(permsManager);
	}

	@Override
	public void init() {
		
	}

	@Override
	public void onDisable() {
		
	}

}
